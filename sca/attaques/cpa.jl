# /*
#  * File: correction_sca.jl
#  * Project: julia
#  * Created Date: Wednesday December 2nd 2020
#  * Author: Hélène (helene.le-bouder@imt-atlantique.fr)
#  * -----
#  * Last Modified: Wednesday, 2nd December 2020 2:44:12 pm
#  * Modified By: Ronan (ronan.lashermes@inria.fr>)
#  * -----
#  * Copyright (c) 2020 INRIA
#  */

using Pkg
Pkg.activate("sca")

Pkg.instantiate()

using Statistics
using Plots
using NPZ



#définie la sbox
sboxtab=[99 124 119 123 242 107 111 197 48 1 103 43 254 215 171 118 202 130 201 125 250 89 71 240 173 212 162 175 156 164 114 192 183 253 147 38 54 63 247 204 52 165 229 241 113 216 49 21 4 199 35 195 24 150 5 154 7 18 128 226 235 39 178 117 9 131 44 26 27 110 90 160 82 59 214 179 41 227 47 132 83 209 0 237 32 252 177 91 106 203 190 57 74 76 88 207 208 239 170 251 67 77 51 133 69 249 2 127 80 60 159 168 81 163 64 143 146 157 56 245 188 182 218 33 16 255 243 210 205 12 19 236 95 151 68 23 196 167 126 61 100 93 25 115 96 129 79 220 34 42 144 136 70 238 184 20 222 94 11 219 224 50 58 10 73 6 36 92 194 211 172 98 145 149 228 121 231 200 55 109 141 213 78 169 108 86 244 234 101 122 174 8 186 120 37 46 28 166 180 198 232 221 116 31 75 189 139 138 112 62 181 102 72 3 246 14 97 53 87 185 134 193 29 158 225 248 152 17 105 217 142 148 155 30 135 233 206 85 40 223 140 161 137 13 191 230 66 104 65 153 45 15 176 84 187 22]

#--------------------------------------------------------
# sbox
# byte in [0;255]
#--------------------------------------------------------
function sbox(byte)
	return sboxtab[byte+1]#index starts at 1!!!
end


#--------------------------------------------------------
# function : gen_prediction_first_round_byte
# give prediction on the first round of AES on one byte
# byte : text byte
# key byte
#--------------------------------------------------------
function gen_prediction_first_round_byte(text_byte, key_byte)
	p=sbox(xor(text_byte,key_byte))
	p=HW(p)
  	return p
end


#--------------------------------------------------------
# function : Prediction_first_round_byte
# give predictions on the first round of AES on one Text_byte
#--------------------------------------------------------
function Prediction_first_round_byte(text_byte)
	K=round.(Int64,0:255)
  	Pk=map(k ->gen_prediction_first_round_byte(text_byte, k), K)
	return Pk
end
#----------------------------------------------------------


#--------------------------------------------------------
# function : Predictions_first_round_byte
# give predictions on the first round of AES on one byte for n texts
# T matrix of n texts (n*16)
# o number of the targeted byte
#--------------------------------------------------------
function Predictions_first_round_byte(Texts,o)

	n=size(Texts,1)
 # 	P=round.(Int64,zeros(n,256))
	#
	# I=round.(Int64,1:n)
	# map(i -> P[i,:]=Prediction_first_round_byte(Texts[i,o]), I)

	P = hcat(map(t -> Prediction_first_round_byte(t), Texts[:,o])...)'


	return P
end
#----------------------------------------------------------


#--------------------------------------------------------
# Function HW
# computes the HW of a vectors or a matrice
#--------------------------------------------------------
function HW(V)
	hw=map(v -> count_ones(v), V)
	return hw

end
#--------------------------------------------------------


#--------------------------------------------------------
# function : bit_i
# give the i th bit of a number X
# X a number
# b the bit
#--------------------------------------------------------
function bit_b(X,b)

	P=bits(X)

	p=P[end:-1:end-8]

 	if(p[b]=='1')
 		pb=1
 	else
 		pb=0
 	end

  	return pb
end
#----------------------------------------------------------




#---------------------------------------------------------------------------------------
# Function : CPA_K
# return the best guess K
# work for mia too
#---------------------------------------------------------------------------------------
function CPA_K(cpa)
	n=size(cpa,2)
	maxi=map(i-> maximum(cpa[:,i]) ,1:n)
	k=argmax(maxi)-1
	return k

end


#charger la matrice de textes
T=npzread("pts.npy")


#charger la matrice de courbes
M=npzread("traces.npy")

#M= convert(Array,M_df)

key=[-1 for _ in 1:16]

for octet=1:16
	P= Predictions_first_round_byte(T,octet)
	cpa=cor(M,P)
	replace!(cpa, NaN=>0)

	plot(cpa,xlabel="temps",ylabel="correlation",legend=:false)

	# sauvegarde la figure
	savefig("courbes-octet_$(octet).pdf")
	cpa_abs=abs.(cpa)

	key[octet]=CPA_K(cpa_abs)

end
