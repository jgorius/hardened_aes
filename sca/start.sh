#!/usr/bin/env nix-shell
#!nix-shell ../default.nix -i python3

# just turn on the target

import chipwhisperer as cw
scope = cw.scope()
target = cw.target(scope)
scope.default_setup()
print(scope.get_serial_ports())