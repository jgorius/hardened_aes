with import <nixpkgs> {};

let
     pkgs = import (builtins.fetchGit {          
         name = "pinned-nixpkgs";                                                 
         url = "https://github.com/nixos/nixpkgs/";                       
         ref = "refs/heads/nixos-22.05";                     
         rev = "7d7622909a38a46415dd146ec046fdc0f3309f44";                                            
     }) {
     };                                                                           
in

let
  chipwhisperer_pkg = ps: ps.callPackage ./chipwhisperer.nix {};
  pythonEnv = pkgs.python310.withPackages (ps: [
    ps.matplotlib
    ps.tqdm
    ps.numpy
    ps.pycrypto
    (chipwhisperer_pkg ps)
  ]);
in

 # Make a new "derivation" that represents our shell
pkgs.pkgsCross.arm-embedded.mkShell {
  name = "dev-env";

  nativeBuildInputs = with pkgs; [
    pythonEnv
    bash
    libusb1
    julia-bin
    # jupyter
  ];

  # The packages in the `buildInputs` list will be added to the PATH in our shell
  buildInputs = [
   
  ];
}