{ lib
, fetchPypi
, buildPythonPackage
, pythonOlder
, tqdm
, libusb1
, pyserial
, cython
, ecpy
, pyusb
, numpy
, scipy
, fastdtw
, configobj
}:

buildPythonPackage rec {
  pname = "chipwhisperer";
  version = "5.5.2";
  # version = "5.6.1";

  disabled = pythonOlder "3.8";

  src = fetchPypi {
    inherit pname version;
    # sha256 = "sha256-Gi12wxSOEkqjVWklYIoNEQGox6C5aFSQMikG0rf/ROs=";
    sha256 = "sha256-amUzHZ2EWCrKRm4FowPUrxYhcI3ifLn3bxAo/T7SXoQ=";
  };

  propagatedBuildInputs = [
    tqdm
    pyserial
    cython
    ecpy
    pyusb
    numpy
    scipy
    fastdtw
    configobj
    libusb1
  ];

  doCheck = false;

  pythonImportsCheck = [ "chipwhisperer" ];

  meta = with lib; {
    description = "ChipWhisperer Side-Channel Analysis Tool";
    homepage = "https://github.com/newaetech/chipwhisperer";
    license = licenses.gpl3;
  };
}
